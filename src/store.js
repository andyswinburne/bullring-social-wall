import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';
import rootReducer from './reducers/index';
import { postsNormaliser } from './middleware/normaliser/postsNormaliser';
 

export default createStore(
  rootReducer,
  composeWithDevTools(applyMiddleware(thunk, postsNormaliser))
);