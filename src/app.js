import React, { Component } from 'react';
import Filter from './components/Filter/Filter';
import Grid from './components/Grid/Grid';
import './scss/main.scss';

class App extends Component {
  render() {
    return (
        <React.Fragment>
          <Filter />
          <Grid />
        </React.Fragment>
    );
  }
}

export default App;
