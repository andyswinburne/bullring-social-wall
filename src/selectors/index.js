export const allPosts = state => state.posts.items || [];

export const postCount = state => allPosts(state).length;

export const isLoadingPosts = state => state.posts.loading;

export const getPostsByChannel = (state, channel) => {
    return allPosts(state).filter(post => {
        return post.service_slug === channel
    })
};

export const filterChannel = state => state.filter.channel;

export const navOpen = state => state.filter.navOpen;
