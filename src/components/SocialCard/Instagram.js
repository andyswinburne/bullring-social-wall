import React from 'react';
import { Card } from 'reactstrap';
import Avatar from './Avatar';
import CardImage from './Image';
import Link from '../Link/Link';
import { fromNow } from '../../selectors/date';

const Instagram = ({item}) => {
    const { item_published, item_data } = item;
    const { caption, image, link, link_text } = item_data;
    const avatar = 'images/instagram-logo.svg';
    return (
        <Card>
            <Avatar src={avatar} />
            <CardImage src={image.large} />
            <p className="card__text" dangerouslySetInnerHTML={ {__html: caption} }></p>
            <Link text={link_text} url={link} />
            <p className="card__timestamp">{ fromNow(item_published) }</p>
        </Card>
    );
}

export default Instagram;
