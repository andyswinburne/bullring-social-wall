import React from 'react';
import { Card } from 'reactstrap';
import Avatar from './Avatar';
import CardImage from './Image';
import Link from '../Link/Link';
import { fromNow } from '../../selectors/date';

const Tweet = ({item}) => {
    const { item_published, item_data } = item;
    const { image_url, link, link_text, tweet, user } = item_data;
    const avatar = 'images/twitter-logo.svg';
    return (
        <Card className="card--tweet">
            <Avatar src={avatar} />
            <CardImage src={image_url} />
            <p className="card__username">{ user.username }</p>
            <p className="card__text" dangerouslySetInnerHTML={ {__html: tweet} }></p>
            <Link text={link_text} url={link} />
            <p className="card__timestamp">{ fromNow(item_published) }</p>
        </Card>
    );
}

export default Tweet;
