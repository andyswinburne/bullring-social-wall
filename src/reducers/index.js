import { combineReducers } from 'redux';
import { filter } from './filter';
import { posts } from './posts';

export default combineReducers({ filter, posts });
