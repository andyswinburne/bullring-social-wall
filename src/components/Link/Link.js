import React from 'react';

const Link = ({url, text}) => {
    if (!url || !text) {
        return null;
    }
    return (
        <React.Fragment>
            <a href={url} className="card__link" target="_blank" rel="noopener noreferrer">{text}</a>
        </React.Fragment>
    );
}

export default Link;