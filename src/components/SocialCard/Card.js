import React from 'react';
import Instagram from './Instagram';
import Manual from './Manual';
import Tweet from './Tweet';

const SocialCard = ({item}) => {
    const { service_name } = item;
    const service = (service_name || '').toLowerCase();
    if (service === 'twitter') {
        return (<Tweet item={item} />);
    } else if (service === 'instagram') {
        return (<Instagram item={item} />);
    }
    return (<Manual item={item} />);
}

export default SocialCard;
