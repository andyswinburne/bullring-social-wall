import moment from 'moment';

export const fromNow = (item_published) => moment(item_published, 'YYYY-MM-DD H:m:ss').fromNow();