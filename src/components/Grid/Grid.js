import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { Button, Container, CardColumns } from 'reactstrap';
import SocialCard from '../SocialCard/Card';
import { fetchPostData, loadingPosts } from '../../actions/actions';
import { allPosts, getPostsByChannel, filterChannel, postCount, isLoadingPosts } from '../../selectors';

class Grid extends PureComponent {
    componentDidMount() {
        this.props.fetchPostData();
    }

    loadMorePosts = () => {
        const { count, fetchPostData, loadingPosts } = this.props;
        loadingPosts(true);
        if (count) {
            fetchPostData({offset: count});
        }
    }
    render() {
        const { loading, posts } = this.props;
        const label = loading ? 'Loading' : 'Load More';
        if (posts) {
            return (
                <React.Fragment>
                    <Container fluid className="grid">
                        <CardColumns>
                            { posts.map((item, index) => (
                                <SocialCard key={`${index}-${item.item_id}`} item={item} />    
                            )) }
                        </CardColumns> 
                    </Container>
                    <img className={`loader ${loading ? 'loader--show' : 'loader--hide'}`} src="images/loader.gif" alt="Loading posts" />
                    <Button block
                        className="button button--center"
                        color="danger"
                        disabled={loading}
                        onClick={this.loadMorePosts}>{label}<span className="sr-only">posts</span></Button>
                </React.Fragment>
            )
        }
        return null;
	}
}

const mapDispatchToProps = {
    fetchPostData, loadingPosts
}

const mapStateToProps = (state) => {
    const channel = filterChannel(state);
    const loading = isLoadingPosts(state);
    return {
        count: postCount(state),
        loading,
        posts: (channel ? getPostsByChannel(state, channel) : allPosts(state))
    };
};
  
export default connect(mapStateToProps, mapDispatchToProps)(Grid)
