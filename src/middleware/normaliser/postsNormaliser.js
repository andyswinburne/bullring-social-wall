import * as types from '../../actions/actionTypes';

export const postsNormaliser = store => next => action => {
    switch(action.type) {
        case types.SAVE_POSTS_DATA:
            const items = action.posts.items.map(post => {
                return parsePost(post);
            });
            return next({
                ...action,
                data: {
                    items
                }
            });
        default:
            return next(action);
    }
}

const parsePost = (post) => {
    const { item_data, service_name } = post;

    switch(service_name) {
        case "Manual":
            if (item_data && item_data.text) {
                item_data.text = processManualLinks(item_data.text);
            }
            break;
        case "Twitter":
            if (item_data && item_data.tweet) {
                item_data.tweet = processTweetLinks(item_data.tweet);
            }
            break;
        case "Instagram":
            if (item_data && item_data.caption) {
                item_data.caption = processInstagramLinks(item_data.caption);
            }
            break;
        }
    return post;    
}

const buildLink = (prefix, href, text) => {
    return `${prefix}<a class='card__link' href='${href}' target='_blank'>${text}</a>`;
}

const processManualLinks = text => {
    let exp = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/gi;
    text = text.replace(exp, buildLink('', '$1', '$1'));
    return text;
}

const processTweetLinks = text => {
    let exp = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/gi;
    text = text.replace(exp, buildLink('', '$1', '$1'));
    exp = /(^|\s)#(\w+)/g;
    text = text.replace(exp, buildLink('$1', 'https://twitter.com/hashtag/$2?src=hash', '#$2'));
    exp = /(^|\s)@(\w+)/g;
    text = text.replace(exp, buildLink('$1', 'https://www.twitter.com/$2', '@$2'));
    return text;
}

const processInstagramLinks = text => {
    let exp = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/gi;
    text = text.replace(exp, buildLink('', '$1', '$1'));
    exp = /(^|\s)#(\w+)/g;
    text = text.replace(exp, buildLink('$1', 'https://www.instagram.com/explore/tags/$2', '#$2'));
    exp = /(^|\s)@(\w+)/g;
    text = text.replace(exp, buildLink('$1', 'https://www.instagram.com/$2', '@$2'));
    return text;
}
