import React, { Component } from 'react';
import { connect } from 'react-redux';
import { clearChannelFilter, fireChannelFilter, toggleNavigation } from '../../actions/actions';
import { filterChannel, navOpen } from '../../selectors';


class Filter extends Component {
    filter(ev, channel) {
        ev.preventDefault();
        this.props.toggleNavigation(false);
        if (this.props.channel == channel) {
            return this.props.clearChannelFilter();
        }
        return this.props.fireChannelFilter(channel);
    }

    toggleNav(open) {
        return this.props.toggleNavigation(open);
    }

    filterItemClass(channel, selected) {
        let className = 'filter__item__link';
        if (selected === channel) {
            className += ' filter__item__link--selected';
        }
        return className;
    }

    render() {
        const { channel, navOpen } = this.props;
        return (
            <nav className="filter">
                <span id="filter_description" className="sr-only">
                    Choose a social media channel
                </span>
                <button onClick={() => this.toggleNav(!navOpen)}
                    aria-labelledby="filter_description filter_button"
                    id="filter_button"
                    className="filter__button">
                    <img className="filter__button__logo" src="images/menu.svg" alt="Filter icon" aria-hidden="true"/>
                </button>
                <ul className={`filter__menu ${navOpen ? 'filter--open' : 'filter--closed'}`}
                    aria-labelledby="filter_description">
                    <li className="filter__item"><a className={this.filterItemClass('manual', channel)} href="/" onClick={(e) => this.filter(e, 'manual')}>Manual</a></li>
                    <li className="filter__item"><a className={this.filterItemClass('instagram', channel)} href="/" onClick={(e) => this.filter(e, 'instagram')}>Instagram</a></li>
                    <li className="filter__item"><a className={this.filterItemClass('twitter', channel)} href="/" onClick={(e) => this.filter(e, 'twitter')}>Twitter</a></li>
                </ul>
            </nav>
        );
    }
}

const mapDispatchToProps = {
    fireChannelFilter, clearChannelFilter, toggleNavigation
}

const mapStateToProps = (state) => {
    return {
        channel: filterChannel(state),
        navOpen: navOpen(state)
    };
};
  
export default connect(mapStateToProps, mapDispatchToProps)(Filter)