import React from 'react';
import { Card } from 'reactstrap';
import Avatar from './Avatar';
import CardImage from './Image';
import Link from '../Link/Link';
import { fromNow } from '../../selectors/date';

const Manual = ({item}) => {
    const { item_published, item_data } = item;
    const { image_url, link, link_text, text } = item_data;

    let avatar = 'images/aff-logo.svg';
    return (
        <Card>
            <Avatar src={avatar} primary />
            <CardImage src={image_url} />
            <p className="card__text">{ text }</p>
            <Link text={link_text} url={link} />
            <p className="card__timestamp">{ fromNow(item_published) }</p>
        </Card>
    );
}

export default Manual;
