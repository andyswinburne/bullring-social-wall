# Front End Developer Task


## Notes on implementation

The app was created using React & Redux, this provides a scalable FE build using the redux store to hold post data received from the API. This acts as a cache meaning the API is only hit to fetch data, filtering then being done on data already in the redux store.

* `Bootstrap` was used via Reactstrap for the card and card column components primarily, as this matches the design closely. This could be reviewed with a designer and further customised as required. 
* `Broken images` both manual and Instragm images failed to load. This was due to the WP site for manual images being in maintenance mode and the Instram CDN image path now being different.
* `Placeholder images` used to handle broken images by detecting the failure and loading in a sample AFF placeholder image.
* `Mock data` a mock JSON file was used to test with valid image URLs and different relative time stamps. Access using {url}/?env=mock E.g. [https://bullring-social-wall.herokuapp.com/?env=mock](https://bullring-social-wall.herokuapp.com/?env=mock)
* `Lazy loaded images` using `vanilla-lazyload` and a LazyImage component to faciliate this
* `Loading` spinner and button state is changed while data is being loaded, this would be checked with the designer regarding UI / UX
* `Icons` from [Flaticon](https://www.flaticon.com) 

## Known issues / queries or improvements
* `Broken images` as mentioned above, this would be checked with the BE developer. Especially regarding the Instragram CDN to see if this could be corrected or mapped to the correct URL.
* `Ordering of posts` the posts are not ordered by date in the API response, this would be preferred to save reordering the posts within the FE store
* `Posts order on 3n page` as the API data response is the same for each page due to pagination being mocked, this causes a bug with every 3rd page of results. As the masonary layout reorders to posts to fit them in the grid, meaning you lost the post order with the same posts repeated. In a live environment this would be unlikely to happen due to varying data and card sizes, mock JSON or API could be updated to further test this.
* `Dynamic channel` the channels (Manual, Twitter & Instagram) are currently hardcoded and could be dynamically fetched form the API and have more dynamic handling of these witih the components. Possibly having a single generic socialCard component rather than channel specific.
* `Unit testing` is not currently in place, but this could be added to logically test the redux normalisers and reduces, plus shallow rending of React components to test these
* `Normalised data` could be extended to reduce the amound of data in the redux store and only store required data
* `Fetch API` polyfill would needed to be added for older browsers depending on scope of browser support