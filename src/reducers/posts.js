import { SAVE_POSTS_DATA, LOADING_POSTS_DATA } from '../actions/actionTypes';

const initialState = { items: [], loading: true };

export function posts(posts = initialState, action = {}) {
  switch (action.type) {
    case SAVE_POSTS_DATA:
      if (posts !== initialState) {
        return { items: posts.items.concat(action.data.items) };
      }
      return { ...action.data };
    case LOADING_POSTS_DATA:
      const { loading } = action;
      return { ...posts, loading };
    default:
      return posts;
  }
}
