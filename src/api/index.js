let endpoint = 'https://private-cc77e-aff.apiary-mock.com/posts';
if (window.location.href.includes("env=mock")) {
    endpoint = window.origin + '/mock/posts.json';
}

export const getPosts = (params) => {
    let url = new URL(endpoint);
    if (params) {
        Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));
    }

    return fetch(url)
        .then((response) => {
            if (response.status !== 200) {
                console.log('Problem fetching posts. Status Code: ' + response.status);
                return;
            }
            return response.json()
        }).catch(function(ex) {
            console.log(`${endpoint} parsing failed`, ex);
        })
}