import React from 'react';

const CardImage = ({src}) => {
    if (!src) return null;
    return (
        <img
            className="card-img-top"
            src={src}
            onError={(e) => {
                e.target.onerror = null;
                e.target.src="https://via.placeholder.com/400x400?text=AFF"
            }}
        />
    )
};

export default CardImage;
