import React from 'react';

const Avatar = ({src, primary = null}) => {
    if (!src) return null;
    let classes = "card__avatar";
    if (primary) {
        classes += " card__avatar--primary";
    }
    return (
        <div className={classes}>
            <img className="card__avatar__img" alt="" src={src} />
        </div>
    )
};

export default Avatar;
