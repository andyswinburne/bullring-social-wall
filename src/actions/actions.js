import * as types from './actionTypes';
import { getPosts } from '../api';

export const fetchPostData = (params) => {
    return (dispatch) => {
        getPosts(params).then(
            posts => dispatch(savePostsData(posts)),
            error => dispatch(errorPostsData(error)),
            loadingPosts(false)
        )
    };
};

export const savePostsData = (posts) => {
    return {
        type: types.SAVE_POSTS_DATA,
        posts
    };
}

export const errorPostsData = (error) => {
    return {
        type: types.ERROR_POSTS_DATA,
        error
    };
}

export const fireChannelFilter = (channel) => {
    return {
        type: types.FIRE_CHANNEL_FILTER,
        channel
    };
}

export const clearChannelFilter = () => {
    return {
        type: types.CLEAR_CHANNEL_FILTER,
    };
}

export const toggleNavigation = (navOpen) => {
    return {
        type: types.TOGGLE_NAVIGATION,
        navOpen
    };
}

export const loadingPosts = (isLoading) => {
    return {
        type: types.LOADING_POSTS_DATA,
        loading: isLoading
    };
}