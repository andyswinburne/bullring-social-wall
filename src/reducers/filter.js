import { FIRE_CHANNEL_FILTER, CLEAR_CHANNEL_FILTER, TOGGLE_NAVIGATION } from '../actions/actionTypes';

export function filter(state = { channel: null, navOpen: false}, action = {}) {
  switch (action.type) {
    case FIRE_CHANNEL_FILTER:
      const { channel } = action;
      return { ...state, channel };
    case CLEAR_CHANNEL_FILTER:
      return { ...state, channel: null };
    case TOGGLE_NAVIGATION:
    const { navOpen } = action;
      return { ...state, navOpen };
    default:
      return state;
  }
}
